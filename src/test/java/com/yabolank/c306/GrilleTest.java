package com.yabolank.c306;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests unitaire pour l'implémentation d'une Grille.
 *
 * @author :  <A HREF="mailto:lankoande.yabo.dev@gmail.com">
 * Yienouyaba LANKOANDE (YaboLANK)</A>
 * @version : 1.0
 * @since : 26/03/2020 à 10:08
 */
public class GrilleTest {

    private Grille grille;
    /**
     * Grille non résolue.
     */
    private char[][] tableau;
    /**
     * Grille résolue.
     */
    private char[][] solution;

    private char[][] tableauAvecDoublons;

    @BeforeEach
    private void initialiser() {
        tableau = new char[][]{
                {'8', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '1'},
                {Grille.EMPTY, Grille.EMPTY, '3', '6', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '4'},
                {Grille.EMPTY, '7', Grille.EMPTY, Grille.EMPTY, '9', Grille.EMPTY, '2', Grille.EMPTY, '8'},
                {Grille.EMPTY, '5', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '7', Grille.EMPTY, Grille.EMPTY, '2'},
                {Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '4', '5', '7', Grille.EMPTY, '3'},
                {Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '1', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '3', '7'},
                {Grille.EMPTY, Grille.EMPTY, '1', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '6', '9'},
                {Grille.EMPTY, Grille.EMPTY, '8', '5', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '1', '6'},
                {Grille.EMPTY, '9', Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, Grille.EMPTY, '4', Grille.EMPTY, '5'}
        };

        solution = new char[][]{
                {'8', '1', '2', '7', '5', '3', '6', '4', '9'},
                {'9', '4', '3', '6', '8', '2', '1', '7', '5'},
                {'6', '7', '5', '4', '9', '1', '2', '8', '3'},
                {'1', '5', '4', '2', '3', '7', '8', '9', '6'},
                {'3', '6', '9', '8', '4', '5', '7', '2', '1'},
                {'2', '8', '7', '1', '6', '9', '5', '3', '4'},
                {'5', '2', '1', '9', '7', '4', '3', '6', '8'},
                {'4', '3', '8', '5', '2', '6', '9', '1', '7'},
                {'7', '9', '6', '3', '1', '8', '4', '5', '2'}
        };

        tableauAvecDoublons = new char[][]{
                {'8', '1', '2', '5', '5', '3', '6', '4', '9'},
                {'9', '4', '3', '6', '8', '2', '1', '7', '5'},
                {'6', '7', '5', '4', '9', '1', '2', '8', '3'},
                {'1', '5', '4', '2', '3', '7', '8', '9', '6'},
                {'3', '6', '9', '8', '4', '5', '7', '2', '1'},
                {'2', '8', '7', '1', '6', '9', '5', '3', '4'},
                {'5', '2', '1', '9', '7', '4', '3', '6', '8'},
                {'4', '3', '8', '5', '2', '6', '9', '1', '7'},
                {'7', '9', '6', '3', '1', '8', '4', '5', '2'}
        };

        grille = new GrilleImpl(tableau);
    }

    @Test
    public void testGetValue() {
        Grille grille = new GrilleImpl(tableau);
        char c = grille.getValue(0, 0);
        Assertions.assertEquals('8', c);
        try {
            char c3 = grille.getValue(20, 20);
            fail("Si x ou y sont hors bornes (0-8)");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testSetValue() {
        grille.setValue(0, 0, '5');
        Assertions.assertEquals(grille.getValue(0, 0), '5');
    }

    @Test
    public void testValeursHorsBornes() {
        try {
            grille.setValue(20, 20, '2');
            fail("Si x ou y sont hors bornes (0-8)");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            grille.setValue(1, 20, '2');
            fail("Si x ou y sont hors bornes (0-8)");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            grille.setValue(23, 2, '2');
            fail("Si x ou y sont hors bornes (0-8)");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testValeursInterdites() {
        try {
            grille.setValue(0, 1, 'X');
            fail("Value n'est pas un caractere autorise");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            grille.setValue(0, 1, '5');
            fail("Valeur existe deja");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testComplete() {
        grille = new GrilleImpl(solution);
        Assertions.assertTrue(grille.complete());

        grille = new GrilleImpl(tableau);
        Assertions.assertFalse(grille.complete());

        grille = new GrilleImpl(solution);
        grille.setValue(0, 0, Grille.EMPTY);
        Assertions.assertFalse(grille.complete());

        grille = new GrilleImpl(tableauAvecDoublons);
        Assertions.assertFalse(grille.complete());
    }

    @Test
    public void testPossible() {
        grille = new GrilleImpl(tableau);
        Assertions.assertFalse(grille.possible(0, 1, '8'));
        Assertions.assertFalse(grille.possible(0, 1, '8'));
        Assertions.assertFalse(grille.possible(1, 1, '8'));
        try {
            boolean b = grille.possible(-1, 1, '8');
            fail("Valeur negative");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            boolean b = grille.possible(1, -11, '8');
            fail("Valeur negative");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testDimension() {
        grille = new GrilleImpl(tableau);
        Assertions.assertEquals(9, grille.getDimension());
    }

    @Test
    public void testParsing() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku3.txt"), grille);
        Assertions.assertTrue(grille.complete());
        Assertions.assertNotNull(grille.toString());
    }

    @Test
    public void testResolvingS1() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku1.txt"), grille);
        grille.resolve();
        Assertions.assertTrue(grille.complete());
    }

    @Test
    public void testResolvingS2() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku2.txt"), grille);
        grille.resolve();
        Assertions.assertTrue(grille.complete());
    }

    @Test
    public void testResolvingS16() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku16.txt"), grille);
        grille.resolve();
        Assertions.assertTrue(grille.complete());
        System.out.println(grille.toString());
    }

    @Test
    public void testResolvingS12C() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku12c.txt"), grille);
        grille.resolve();
        Assertions.assertTrue(grille.complete());
        System.out.println(grille.toString());
    }

    @Test
    public void testExistenceEnLigneOuColonne() throws IOException {
        grille = new GrilleImpl(new char[9][9]);
        GrilleParser.parse(new File("src\\test\\resources\\sudoku1.txt"), grille);

        Assertions.assertTrue(grille.existeEnLigneOuColonne(0, '9', 'l'));
        Assertions.assertTrue(grille.existeEnLigneOuColonne(3, '7', 'c'));

        Assertions.assertFalse(grille.existeEnLigneOuColonne(1, '3', 'l'));
        Assertions.assertFalse(grille.existeEnLigneOuColonne(4, '5', 'c'));

    }


}