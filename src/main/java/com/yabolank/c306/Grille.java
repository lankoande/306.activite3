package com.yabolank.c306;

/**
 * Interface d'une grille de sudoku.
 *
 * @author :  <A HREF="mailto:lankoande.yabo.dev@gmail.com">
 * Yienouyaba LANKOANDE (YaboLANK)</A>
 * @version : 1.0
 * @since : 27/03/2020 à 12:08
 */
public interface Grille {
    /**
     * Caractere de case vide.
     */
    char EMPTY = '@';

    /**
     * Caractere possible a mettre dans la grille.
     * <p>
     * pour une grille 9x9 : 1..9
     * <p>
     * pour une grille 16x16: 0..9-a..f
     */
    char[] POSSIBLE = new char[]{'1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * Calcul la dimension de la grille.
     *
     * @return largeur/hauteur de la grille
     */
    int getDimension();

    /**
     * Affecte une valeur dans la grille.
     *
     * @param x     position x dans la grille
     * @param y     position y dans la grille
     * @param value valeur a mettre dans la case
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     * @throws IllegalArgumentException si la valeur est interdite aux vues des
     *                                  autres valeurs de la grille
     * @throws IllegalArgumentException si value n'est pas un caractere autorise
     *                                  ('1',...,'9')
     */
    void setValue(int x, int y, char value) throws IllegalArgumentException;

    /**
     * Recupere une valeur de la grille.
     *
     * @param x position x dans la grille
     * @param y position y dans la grille
     * @return valeur dans la case x,y
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     */
    char getValue(int x, int y) throws IllegalArgumentException;

    /**
     * Test si une grille est terminee.
     *
     * @return true si la grille est complete
     */
    boolean complete();

    /**
     * Test si une valeur est possible dans la grille par rapport a ce qu'elle
     * contient deja.
     *
     * @param x     position x dans la grille
     * @param y     position y dans la grille
     * @param value valeur a mettre dans la case
     * @return <code>true</code> si la valeur est possible et <code>false</code>
     * si non
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     * @throws IllegalArgumentException si value n'est pas un caractere autorise
     *                                  ('1',...,'9',..)
     */
    boolean possible(int x, int y, char value) throws IllegalArgumentException;

    /**
     * Essaye la résolution du plateau
     * en utilisant la méthode récursive.
     *
     * @return <code>true</code>,
     * si la grille a été résolu et <code>false</code> si non.
     */
    boolean resolve();

    /**
     * Vérifie si une valeur est existe déjà dans une ligne.
     *
     * @param index Ligne ou la colonne suivant la
     *              valeur du paramètre <code>comparerLesLignes</code>
     * @param val   Valeur
     * @param axe   ( l ou c ) Prendre en compte les lignes
     * @return <code>true</code>, si la valeur existe déjà dans
     * la ligne <code>l</code>
     */
    boolean existeEnLigneOuColonne(
            int index, char val, char axe);

    /**
     * Vérifie si la valeur  existe dans sa cellule 3x3.
     *
     * @param ligne   coordonnée X
     * @param colonne coordonnée Y
     * @param val     Valeur
     * @return <code>true</code> si la valeur
     * <code>val</code> à ajouter appartient déjà à une matrice 3x3
     */
    boolean appartient3x3(
            int ligne, int colonne, char val);
}
