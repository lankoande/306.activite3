package com.yabolank.c306;

/**
 * Implémentation de la Grille de jeu.
 *
 * @author :  <A HREF="mailto:lankoande.yabo.dev@gmail.com">
 * Yienouyaba LANKOANDE (YaboLANK)</A>
 * @version : 1.0
 * @since : 27/03/2020 à 12:08
 */

public class GrilleImpl implements Grille {

    /**
     * Plateau de jeu.
     */
    private final char[][] plateau;

    /**
     * Constructeur de la grille.
     *
     * @param p : Le plateau
     */
    public GrilleImpl(final char[][] p) {
        this.plateau = p;
    }

    /**
     * Calcul la dimension de la grille.
     *
     * @return largeur/hauteur de la grille
     */
    public final int getDimension() {
        return plateau.length;
    }

    /**
     * Affecte une valeur dans la grille.
     *
     * @param x     position x dans la grille
     * @param y     position y dans la grille
     * @param value valeur a mettre dans la case
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     * @throws IllegalArgumentException si la valeur est interdite aux vues des
     *                                  autres valeurs de la grille. Par exemple
     *                                  en cas de doublons, etc.
     * @throws IllegalArgumentException si value n'est pas un caractere autorise
     *                                  ('1',...,'9')
     */
    public final void setValue(final int x, final int y, final char value)
            throws IllegalArgumentException {

        if (value == EMPTY || possible(x, y, value)) {
            plateau[x][y] = value;
        } else {
            throw new IllegalArgumentException(
                    "Valeur non autorisée : " + value);
        }
    }

    /**
     * Valider les coordonnées d'un point dans la grille.
     *
     * @param x abscisse
     * @param y ordonnée
     * @throws IllegalArgumentException si le point est invalide
     */
    private void valider(final int x, final int y) {
        if (x < 0 || x >= plateau[0].length) {
            throw new IllegalArgumentException("X hors bornes (0-"
                    + plateau[0].length + ")");
        }

        if (y < 0 || y >= plateau.length) {
            throw new IllegalArgumentException("Y hors bornes (0-"
                    + plateau.length + ")");
        }
    }

    /**
     * Vérifie la validité un caractère.
     * J'autorise les valeurs <code>EMPTY</code>
     * Donc on peut écraser une valeur présaisie par une valeur vide.
     *
     * @param val : Le caractère à vérifier
     * @throws IllegalArgumentException si la valeur n'est pas autorisée
     */
    private void valider(final char val) {
        if (val != EMPTY) {
            boolean contient = false;
            for (char c : POSSIBLE) {
                if (c == val) {
                    contient = true;
                    break;
                }
            }
            if (!contient) {
                throw new IllegalArgumentException("Valeur"
                        + " non autorisée : <" + val + ">");
            }
        }
    }

    /**
     * Recupere une valeur de la grille.
     *
     * @param x position x dans la grille
     * @param y position y dans la grille
     * @return valeur dans la case x,y
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     */
    public final char getValue(final int x, final int y)
            throws IllegalArgumentException {
        valider(x, y);
        return plateau[x][y];
    }

    /**
     * Test si une grille est terminee.
     *
     * @return true si la grille est complete
     */
    public final boolean complete() {
        for (int i = 0; i < plateau.length - 1; i++) {
            for (int j = i; j < plateau[i].length; j++) {
                int occurence = occurence(plateau[i], plateau[i][j]);
                if (plateau[i][j] == EMPTY) {
                    return false;
                }
                if (occurence > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Recherche le nombre d'occurence d'une chaine dans un tableau.
     *
     * @param tab Tableau contenant la liste des caractères
     * @param val Caractère à rechercher
     * @return le nombre d'occurence
     */
    public final int occurence(final char[] tab, final char val) {
        int count = 0;
        for (int i = tab.length - 1; i >= 0; i--) {
            if (tab[i] == val) {
                count++;
            }
        }
        return count;
    }

    /**
     * Test si une valeur est possible dans la grille par rapport a ce qu'elle
     * contient deja.
     *
     * @param x   position x dans la grille
     * @param y   position y dans la grille
     * @param val valeur a mettre dans la case
     * @return <code>true</code> si la valeur est possible et <code>false</code>
     * si non
     * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
     * @throws IllegalArgumentException si value n'est pas un caractere autorise
     *                                  ('1',...,'9',..)
     * @see #appartient3x3(int, int, char)
     * @see #existeEnLigneOuColonne(int, char, char)
     * @see #valider(char)
     * @see #valider(int, int)
     */
    public final boolean possible(final int x, final int y, final char val)
            throws IllegalArgumentException {
        this.valider(x, y);
        this.valider(val);

        return !existeEnLigneOuColonne(x, val, 'l')
                && !existeEnLigneOuColonne(y, val, 'c')
                && !appartient3x3(x, y, val);
    }

    /**
     * Vérifie si une valeur est existe déjà dans une ligne.
     *
     * @param index Ligne ou la colonne suivant la
     *              valeur du paramètre <code>comparerLesLignes</code>
     * @param val   Valeur
     * @param axe   ( l ou c ) Prendre en compte les lignes
     * @return <code>true</code>, si la valeur existe déjà dans
     * la ligne <code>l</code>
     */
    public final boolean existeEnLigneOuColonne(
            final int index, final char val, final char axe) {
        if (axe == 'l') {
            for (int i = 0; i < plateau.length; i++) {
                if (plateau[index][i] == val) {
                    return true;
                }
            }
        } else {
            for (char[] chars : plateau) {
                if (chars[index] == val) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Vérifie si la valeur  existe dans sa cellule 3x3.
     *
     * @param ligne   la coordonnée X
     * @param colonne la coordonnée Y
     * @param val     La valeur
     * @return <code>true</code> si la valeur
     * <code>val</code> à ajouter appartient déjà à une matrice 3x3
     */
    public final boolean appartient3x3(
            final int ligne, final int colonne, final char val) {
        final int m = 3;
        int r = ligne - ligne % m;
        int c = colonne - colonne % m;
        for (int i = r; i < r + m; i++) {
            for (int j = c; j < c + m; j++) {
                if (plateau[i][j] == val) {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * Essayer la résolution du plateau
     * en utilisant la méthode récursive.
     *
     * @return <code>true</code>, si la grille a été résolu
     * et <code>false</code> si non.
     */
    public final boolean resolve() {
        for (int i = 0; i < plateau.length; i++) {
            for (int j = 0; j < plateau.length; j++) {
                // Si la cellule est vide
                if (plateau[i][j] == EMPTY) {
                    // J'essaie toutes les valeurs possibles.
                    // Pour la case, en question
                    for (int k = 0; k < plateau.length; k++) {
                        if (possible(i, j, POSSIBLE[k])) {
                            // Cette valeur, respect les contraintes de sudoku
                            plateau[i][j] = POSSIBLE[k];
                            if (resolve()) {
                                return true;
                            } else {
                                setValue(i, j, EMPTY);
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true; // Normalement, c'est OK, si on en vient à là !
    }

    /**
     * Redéfinition de la méthode
     * pour afficher la  grille.
     *
     * @return Un affichage de la grille sous forme de tableau
     */
    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (char[] chars : plateau) {
            for (char aChar : chars) {
                sb.append(aChar)
                        .append('\t');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
