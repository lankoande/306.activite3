package com.yabolank.c306;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Classe de parsing.
 */
public final class GrilleParser {

    /**
     * Constructeur privé.
     */
    private GrilleParser() {

    }

    /**
     * Méthode utilitaire permettant de parser
     * le contenu d'un fichier.
     *
     * @param in     Le flux d'entrée
     * @param grille Le plateau de jeu
     * @throws IOException En cas, d'erreur de lecture ou de
     *                     parsing du fichier.
     * @see #parse(File, Grille)
     */
    public static void parse(
            final InputStream in,
            final Grille grille) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(in, Charset.forName("UTF-8")));
        int dimension = grille.getDimension();
        for (int line = 0; line < dimension; line++) {
            String ligne = reader.readLine();
            if (ligne == null || ligne.length() != dimension) {
                throw new EOFException("format incorrect");
            }
            for (int i = 0; i < dimension; i++) {
                grille.setValue(line, i, ligne.charAt(i));
            }

        }
        reader.close();
    }

    /**
     * Variante de la fonction de parsing.
     *
     * @param f      Le fichier
     * @param grille Le plateau de jeu
     * @throws IOException En cas, d'erreur de lecture ou de
     *                     parsing du fichier.
     * @see #parse(InputStream, Grille)
     */
    public static void parse(
            final File f,
            final Grille grille) throws IOException {
        parse(new FileInputStream(f), grille);
    }

}
